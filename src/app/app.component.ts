import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators,FormArray } from '@angular/forms';
import { PasswordValidator } from './shared/user-name.validators';
import { RegistrationService } from './registration.service';
// import { error } from 'console';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {

  registrationForm: FormGroup;

  
  get userName(){
    return this.registrationForm.get('userName')
  }

  get email(){
    return this.registrationForm.get('email')
  }

  get alternateEmails() {
    return this.registrationForm.get('alternateEmails') as FormArray;
  }

  addAlternateEmails(){
    this.alternateEmails.push(this.fb.control(''));
  }


  
  constructor(private fb:FormBuilder, private _registrationService: RegistrationService){}


  ngOnInit() {
    
    this.registrationForm = this.fb.group({
      userName: ['',[Validators.required,Validators.minLength(3),]],
      email: [''],
      subscribe:[false],
      password: [''],
      confirmPassword: [''],
      address: this.fb.group({
        city:[''],
        State:[''],
        Postalcode:['']
      }),
      alternateEmails: this.fb.array([])
    }, { validator : PasswordValidator });

    this.registrationForm.get('subscribe')?.valueChanges
       .subscribe(checkedValue => {
         const email = this.registrationForm.get('email');
         if (checkedValue) {
           email?.setValidators(Validators.required);
         }else {
           email?.clearValidators();
         }
         email?.updateValueAndValidity()
       })
  }

  
  // registrationForm = new FormGroup({
  //   userName : new FormControl(''),
  //   password : new FormControl(''),
  //   confirmPassword : new FormControl(''),
  //   address : new FormGroup({
  //     city : new FormControl(''),
  //     State : new FormControl(''),
  //     Postalcode : new FormControl('')
  //   })
  // })

  loadAPIdata(){
    this.registrationForm.patchValue({
      userName: 'Raj',
      password: 'asi',
      confirmPassword: 'asi',
      // address:{
      //   city: 'madu',
      //   State: 'tam',
      //   Postalcode: '252'
      // }
    });
  }
  onSubmit(){
    console.log(this.registrationForm.value);
    this._registrationService.register(this.registrationForm.value)
     .subscribe(
       response => console.log('!success',response),
       error => console.log('Error!',error)
     );
  }
}
